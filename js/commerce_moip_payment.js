/**
 * @file
 * Javascript to generate MoIP hash in PCI-compliant way.
 */

(function ($) {
  Drupal.behaviors.moip = {
    attach: function (context, settings) {
      $(document).ready(function() {
        $(".moip-change").change(function(e) {
          Drupal.behaviors.moip.moipValidate(context,settings);
        });
      });
      $('.moip-number,.moip-code').on('input propertychange paste', function() {
        Drupal.behaviors.moip.moipValidate(context,settings);
      });
    },
    moipValidate: function (context,settings) {
      var cc = new Moip.CreditCard({
        number  : $(".moip-number").val(),
        cvc     : $(".moip-code").val(),
        expMonth: $(".moip-exp-month").val(),
        expYear : $(".moip-exp-year").val(),
        pubKey  : settings.moipPublicKey
      });
      if( cc.isValid()){
        $(".moip-encrypted-hash").val(String(cc.hash()));
        $(".moip-number").prop('disabled',true);
        $(".moip-code").prop('disabled',true);
        $(".moip-exp-month").prop('disabled',true);
        $(".moip-exp-year").prop('disabled',true);
      }
      else{
        $(".moip-encrypted-hash").val('');
      }
    }
  }
})(jQuery);
