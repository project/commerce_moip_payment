<?php
/**
 * @file
 * This code is never called, it's just information about to use the hooks.
 */


/**
 * Alter the description of the order sent to MoIP in the payment details.
 *
 * Note: You need to set below the fields from which you pull data from as they're required for MoIP
 * Order items are prepropulated from the ones from the order you'll need to set discount, additions and fees and convert amount from order to BRL
 *
 * $data['birth_date'] = '';  needs to be in format example: 1990-12-24
 * $data['tax_document'] = ''; string fetch data from your field
 * $data['tax_document_type'] = ''; string by default this is CPF fetch data from your field
 * $data['phone']['area_code'] = ''; integer fetch data from your field
 * $data['phone']['number'] = ''; integer fetch data from your field
 * $data['phone']['country_code']; integer by default this is 55 fetch data from your field
 * $data['order']['items']; array with items -- alter amount to be in BRL for sending
 * $data['order']['addition']; add addition fees to the order if needed
 * $data['order']['discount']; add discount to the order if needed
 * $data['order']['shipping_fee']; add shipping fee if needed
 *
 * @param array $data
 *  Data for sending to MoIP
 * @param \stdClass $order
 */
function hook_commerce_moip_payment_prepare_data_alter(&$data, $order) {

}


