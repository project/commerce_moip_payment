<?php
/**
 * Created by PhpStorm.
 * User: dalibor
 * Date: 4/25/18
 * Time: 11:19 PM
 */

/**
 * Form callback: allows the user to void a transaction.
 */
function commerce_moip_payment_void_form($form, &$form_state, $order, $transaction) {
  $form_state['order'] = $order;
  $form_state['transaction'] = $transaction;

  // Load and store the payment method instance for this transaction.
  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
  $form_state['payment_method'] = $payment_method;

  $form['markup'] = array(
    '#markup' => t('Are you sure that you want to void this transaction?'),
  );

  $form = confirm_form($form,
    t('Are you sure that you want to void this transaction?'),
    'admin/commerce/orders/' . $order->order_id . '/payment',
    '',
    t('Void'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit handler: process the void request.
 *
 */
function commerce_moip_payment_void_form_submit($form, &$form_state) {
  $transaction = $form_state['transaction'];
  $order = $form_state['order'];

  if (!commerce_moip_payment_load_library()) {
    drupal_set_message(t('Error canceling payment. Please contact shop admin to proceed.'), 'error');
    drupal_goto('admin/commerce/orders/' . $form_state['order']->order_id . '/payment');
  }

  commerce_moip_payment_void($order,$transaction,FALSE,FALSE);

  $form_state['redirect'] = 'admin/commerce/orders/' . $order->order_id . '/payment';
}


/**
 * @param $order
 * @param $transaction
 * @param bool $moip
 * @param bool $payment
 *
 * @return bool
 */
function commerce_moip_payment_void($order, $transaction, $moip = FALSE, $payment = FALSE){
  //load library
  if(!commerce_moip_payment_load_library()){
    return FALSE;
  }

  //load connection
  if(!$moip instanceof \Moip\Moip){
    $moip = commerce_moip_payment_connection();
  }

  try {
    /** @var \Moip\Resource\Payment $payment */
    if(!$payment instanceof \Moip\Resource\Payment){
      $payment = $moip->payments()->get($transaction->remote_id);
    }

    $payment->cancel();

    //$transaction->payload[REQUEST_TIME] = $payment->jsonSerialize();
    $transaction->remote_status = $payment->getStatus();
    $transaction->message .= '<br />' . t('Voided: @date', array('@date' => format_date(REQUEST_TIME, 'short')));
    // Set the status to failure so that it isn't used for order balance
    // calculations.
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    $transaction->amount = 0;
    commerce_payment_transaction_save($transaction);
  }
  catch (Exception $e) {
    drupal_set_message(t('We received the following error when trying to void the transaction.'), 'error');
    drupal_set_message(check_plain($e->getMessage()), 'error');
    watchdog('commerce_moip_payment', 'Following error received when trying to void transaction @error.', array('@error' => $e->getMessage()), WATCHDOG_NOTICE);
  }
}
