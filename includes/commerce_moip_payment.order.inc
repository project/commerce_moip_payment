<?php
/**
 * @file Used for MoIP orders functions
 */

/**
 * Creates MoIP order based on parameters
 *
 * @param \Moip\Moip $moip
 * @param array $data
 * @param \stdClass $commerce_order
 * @param \Moip\Resource\Customer $customer
 *
 * @return \Moip\Resource\Orders
 */
function commerce_moip_payment_create_order($moip, $data, $commerce_order, $customer){
  $order = $moip->orders()->setOwnId($commerce_order->order_id)
    ->setCustomer($customer);

  //add order items
  foreach ($data['order']['items'] as $item) {
    $order->addItem($item['label'], $item['quantity'], $item['sku'], $item['amount'], $item['category']);
  }

  //check if there is some shipping amount
  if ($data['order']['shipping_amount']) {
    $order->setShippingAmount($data['order']['shipping_amount']);
  }

  //check if there is some addition to price
  if ($data['order']['addition']) {
    $order->setAddition($data['order']['addition']);
  }
  //check if there is some discount to order price
  if ($data['order']['discount']) {
    $order->setDiscount($data['order']['discount']);
  }

  $order->create();
  return $order;
}