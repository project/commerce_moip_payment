<?php
/**
 * @file is used for refund admin form
 *
 */

/**
 * Form callback for processing refunds.
 */
function commerce_moip_payment_refund_form($form, &$form_state, $order, $transaction) {
  $form = array();
  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
  $form_state['order'] = $order;
  $form_state['transaction'] = $transaction;
  $form_state['payment_method'] = $payment_method;

  // Make sure the library is available.
  if (!commerce_moip_payment_load_library()) {
    drupal_set_message(t('Cannot load the MoIP PHP library'), 'error');
    return $form;
  }

  // Make sure we can load the original charge object.
  try {
    $moip = commerce_moip_payment_connection();
    /** @var \Moip\Resource\Payment $payment */
    $payment = $moip->payments()->get($transaction->remote_id);
    $form_state['payment'] = $payment;
  }
  catch (Exception $e) {
    drupal_set_message(t('The original transaction could not be loaded. The error was: @error', ['@error' => $e->getMessage()]), 'error');
    return $form;
  }

  // Calculate the amount left available for a refund.
  $amount_refunded = 0;
  $payment_object = $payment->jsonSerialize();
  if ($payment_object->refunds) {
    foreach ($payment_object->refunds as $refund){
      $amount_refunded += $refund->amount->total;
    }
  }

  $form_state['refunded'] = $amount_refunded;

  $remaining = $payment->getAmount()->total - $amount_refunded;

  $form['amount'] = [
    '#type' => 'textfield',
    '#title' => t('Refund amount'),
    '#description' => t('Enter any amount to refund up to @txn_amount', ['@txn_amount' => commerce_currency_format($remaining, $transaction->currency_code)]),
    '#required' => TRUE,
    '#size' => 8,
  ];

  $form['reason'] = [
    '#type' => 'select',
    '#title' => t('Refund reason'),
    '#description' => t('Select the most appropriate reason for the refund.'),
    '#options' => [
      'requested_by_customer' => t('Requested by customer'),
      'duplicate' => t('Duplicate'),
      'fraudulent' => t('Fraduluent'),
    ],
  ];

  $form['actions'] = [
    '#type' => 'container',
  ];

  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Process refund'),
  ];

  return $form;
}

/**
 * Validation callback for submitting refunds to MoIP.
 */
function commerce_moip_payment_refund_form_validate($form, &$form_state) {
  $transaction = $form_state['transaction'];
  $amount = commerce_currency_decimal_to_amount($form_state['values']['amount'], $transaction->currency_code);

  // Calculate the amount left available for a refund.
  $amount_refunded = !empty($form_state['refunded']) ? $form_state['refunded'] : 0;
  $remaining = $form_state['payment']->getAmount()->total - $amount_refunded;

  // Make sure the refund amount is valid and available.
  if ($amount <= 0 || $amount > $remaining || !is_numeric($amount)) {
    form_set_error('amount', t('Please enter a valid return amount that is less than or equal to the remaining balance available for refund of @remaining.', array('@remaining' => commerce_currency_format($remaining, $transaction->currency_code))));
  }
}

/**
 * Submit callback for submitting refunds to MoIP.
 */
function commerce_moip_payment_refund_form_submit($form, &$form_state) {

  // Don't rely on form_state objects to be fresh.
  $order = commerce_order_load($form_state['order']->order_id);
  $transaction = commerce_payment_transaction_load($form_state['transaction']->transaction_id);
  $payment_method = $form_state['payment_method'];
  global $user;

  // Make sure the library is available.
  if (!commerce_moip_payment_load_library()) {
    drupal_set_message(t('Cannot load the MoIP library'), 'error');
    return FALSE;
  }

  // Create the refund object.
  $data = array(
    'charge' => $transaction->remote_id,
    'amount' => commerce_currency_decimal_to_amount($form_state['values']['amount'], $transaction->currency_code),
    'reason' => $form_state['values']['reason'],
  );
  /** @var \Moip\Resource\Payment $payment */
  $payment = $form_state['payment'];

  try {
    /** @var \Moip\Resource\Refund $refund */
    $refund = $payment->refunds()->creditCardPartial($data['amount']);

    if (is_object($refund) && $refund->getStatus() === 'COMPLETED') {
      // Copy the refund object into our own payload so we don't save API keys
      // included in the response object.
      $refund_object = $refund->jsonSerialize();

      $payload = array(
        'id' => $refund_object->id,
        'amount' => $refund_object->amount->total,
        'currency' => $refund_object->amount->currency,
        'reason' => $data['reason'],
        'created' => $refund_object->createdAt,
        'type' => $refund_object->type,
        'status' => $refund_object->status,
        'refundingInstrument'=> $refund_object->refundingInstrument->method,
      );

      // Create the new commerce payment transation and set appropriate values.
      $refund_transaction = commerce_payment_transaction_new($transaction->payment_method, $order->order_id);
      $refund_transaction->instance_id = $payment_method['instance_id'];
      $refund_transaction->payload[REQUEST_TIME] = print_r($payload, TRUE);
      $refund_transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $refund_transaction->remote_id = $payment->getId();
      $refund_transaction->message = t('Refund issued.');
      $refund_transaction->remote_status = $refund->getStatus();

      // Save the amount as a negative integer.
      $refund_transaction->amount = $refund_object->amount->total * -1;
      $refund_transaction->currency_code = $refund_object->amount->currency;
      commerce_payment_transaction_save($refund_transaction);

      // Inform the user of the success and redirect them back to payments.
      drupal_set_message(t('Refund processed successfully'));
      $form_state['redirect'] = 'admin/commerce/orders/' . $order->order_id . '/payment';
    }
  }
  catch (Exception $e) {
    commerce_moip_payment_transaction($payment_method,$order,$data,COMMERCE_PAYMENT_STATUS_FAILURE,TRUE,$e->getMessage(),FALSE,$refund);
    drupal_set_message(t('The transaction could not be refunded. The error was: @error',
      array('@error' => $e->getMessage())), 'error');
  }
}
