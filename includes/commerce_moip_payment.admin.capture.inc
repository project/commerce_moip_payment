<?php
/**
 * @form Admin capture form
 */

/**
 * Form callback for capturing a authorized payment.
 *
 * @param $form
 * @param $form_state
 * @param $order
 * @param $transaction
 *
 * @return mixed
 */
function commerce_moip_payment_capture_form($form, &$form_state, $order, $transaction) {
  $form_state['order'] = $order;
  $form_state['transaction'] = $transaction;

  // Load and store the payment method instance for this transaction.
  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
  $form_state['payment_method'] = $payment_method;

  $form = confirm_form($form,
    t('Are you sure you want to perform capture on transaction @remote_id?', ['@remote_id' => $transaction->remote_id]),
    'admin/commerce/orders/' . $order->order_id . '/payment',
    '',
    t('Capture'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Validation handler
 *
 * @param $form
 * @param $form_state
 */
function commerce_moip_payment_capture_form_validate($form, &$form_state) {
  $transaction = $form_state['transaction'];
  // Verify this transaction is authorization only (IN_ANALYSIS,PRE_AUTHORIZED).
  if (!in_array($transaction->remote_status,['IN_ANALYSIS','PRE_AUTHORIZED'])) {
    drupal_set_message(t('This operation can only be done on transactions in following statuses: IN_ANALYSIS , PRE_AUTHORIZED'), 'error');
    drupal_goto('admin/commerce/orders/' . $form_state['order']->order_id . '/payment');
  }

  // If the authorization has expired, display an error message and redirect.
  if (time() - $transaction->created > 86400 * 5) {
    drupal_set_message(t('This authorization has passed its 5 day limit and cannot be captured.'), 'error');
    drupal_goto('admin/commerce/orders/' . $form_state['order']->order_id . '/payment');
  }
}

/**
 * Submit handler
 *
 * @param $form
 * @param $form_state
 */
function commerce_moip_payment_capture_form_submit($form, &$form_state) {
  commerce_moip_payment_capture($form_state['order'], $form_state['transaction']);
  $form_state['redirect'] = 'admin/commerce/orders/' . $form_state['order']->order_id . '/payment';
}


/**
 * Perform capture of MoIP order
 *
 * @param \stdClass $order
 * @param \stdClass $transaction
 * @param bool|\Moip\Moip $moip
 * @param bool|\Moip\Resource\Payment $payment
 *
 * @return bool
 */
function commerce_moip_payment_capture($order, $transaction, $moip = FALSE, $payment = FALSE){
  //load library
  if(!commerce_moip_payment_load_library()){
    return FALSE;
  }

  //load connection
  if(!$moip instanceof \Moip\Moip){
    $moip = commerce_moip_payment_connection();
  }

  try{
    if(!$payment instanceof \Moip\Resource\Payment){
      /** @var \Moip\Resource\Payment $payment */
      $payment = $moip->payments()->get($transaction->remote_id);
      if($payment instanceof  \Moip\Resource\Payment){
        $payment->capture();
      }
    }else{
        $payment->capture();
    }

    $amount = $payment->getAmount();

    if($payment->getStatus() == 'AUTHORIZED'){
      $transaction->payload[REQUEST_TIME] = $payment->getPayments();
      $transaction->remote_status = $payment->getStatus();
      $transaction->message .= '<br />' . t('Capture performed');
      $transaction->message .= '<br />' . t('Captured: @date', array('@date' => format_date(REQUEST_TIME, 'short')));
      $transaction->message .= '<br />' . t('Captured Amount: @amount', array('@amount' => commerce_currency_format($amount->total,$amount->currency)));
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->amount = $amount->total;
      commerce_payment_transaction_save($transaction);
      drupal_set_message('Capture performed');
    }
  }catch (Exception $e){
    drupal_set_message(t('We received the following error when trying to capture the transaction.'), 'error');
    drupal_set_message(check_plain($e->getMessage()), 'error');
    watchdog('commerce_moip_payment', 'Following error received when processing card for capture @moip_error.', array('@moip_error' => $e->getMessage()), WATCHDOG_NOTICE);

    $transaction->payload[REQUEST_TIME] = $e->getTraceAsString();
    $transaction->message .= t('Capture processing error: @moip_error', array('@moip_error' => $e->getMessage()));
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    $transaction->remote_status = 'FAILED';
    commerce_payment_transaction_save($transaction);
  }
  return $payment;
}