<?php

/**
 * @file
 * This module provides MoIP (https://moip.com.br/) payment gateway integration
 * to Commerce.
 */


use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;


define('COMMERCE_MOIP_PAYMENT_DEFAULT_AUTH_TYPE', 'basic');
define('COMMERCE_MOIP_PAYMENT_ENDPOINT', 'sandbox');


/**
 * Implements hook_menu().
 */
function commerce_moip_payment_menu() {
  $items = [];

  // Add a menu item to moip payment transactions that can be refunded.
  $items['admin/commerce/orders/%commerce_order/payment/%commerce_payment_transaction/commerce-moip-refund'] = [
    'title' => 'Refund',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['commerce_moip_payment_refund_form', 3, 5],
    'access callback' => 'commerce_moip_payment_refund_access',
    'access arguments' => [3, 5],
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 1,
    'file' => 'includes/commerce_moip_payment.admin.refund.inc',
  ];

  $items['admin/commerce/orders/%commerce_order/payment/%commerce_payment_transaction/moip-capture'] = [
    'title' => 'Capture',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['commerce_moip_payment_capture_form', 3, 5],
    'access callback' => 'commerce_moip_payment_capture_access',
    'access arguments' => [3, 5],
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 2,
    'file' => 'includes/commerce_moip_payment.admin.capture.inc',
  ];

  $items['admin/commerce/orders/%commerce_order/payment/%commerce_payment_transaction/moip-void'] = [
    'title' => 'Void',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['commerce_moip_payment_void_form', 3, 5],
    'access callback' => 'commerce_moip_payment_void_access',
    'access arguments' => [3, 5],
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 2,
    'file' => 'includes/commerce_moip_payment.admin.void.inc',
  ];

  return $items;
}


/**
 * Implements hook_libraries_info().
 */
function commerce_moip_payment_libraries_info() {
  $libraries['moip-sdk-php'] = [
    'name' => 'MoIP PHP client SDK Library',
    'vendor url' => 'https://moip.com.br/',
    'download url' => 'https://github.com/moip/moip-sdk-php',
    'dependencies' => [],
    'version arguments' => [
      'file' => 'CHANGELOG.md',
      'pattern' => '/(v(\d+)\.(\d+)\.(\d+)(?!\.))/',
    ],
    'files' => [
      'php' => [
        'vendor/autoload.php',
      ],
    ],
    'callbacks' => [
      'post-load' => [
        'commerce_moip_payment_libraries_post_load_callback',
      ],
    ],
  ];
  return $libraries;
}

/**
 * Brings the MoIP php client library into scope
 */
function commerce_moip_payment_load_library() {
  $library = libraries_load('moip-sdk-php');
  if (!$library || empty($library['loaded'])) {
    watchdog('commerce_moip_payment', 'Failure to load MoIP PHP client SDK Library. Please see the Status Report for more.', [], WATCHDOG_CRITICAL);
    return FALSE;
  }
  return TRUE;
}

/**
 * Create connection to moip
 *
 * @return bool|\Moip\Moip
 */
function commerce_moip_payment_connection() {
  commerce_moip_payment_load_library();
  $payment_method = commerce_payment_method_instance_load('commerce_moip_payment|commerce_payment_commerce_moip_payment');

  if (isset($payment_method['settings']['authentication_type'])) {
    $endpoint = $payment_method['settings']['endpoint'] === 'production' ? Moip::ENDPOINT_PRODUCTION : Moip::ENDPOINT_SANDBOX;
    switch ($payment_method['settings']['authentication_type']) {
      case 'basic':
        return new Moip(new BasicAuth($payment_method['settings']['token'], $payment_method['settings']['key']), $endpoint);
      case'oauth':
        return new Moip(new OAuth($payment_method['settings']['access_token']), $endpoint);
        break;
    }
  }

  return FALSE;
}

/**
 * Post-load callback for the MoIP PHP client SDK Library Library.
 *
 * @param array $library
 *   An array of library information.
 * @param string $version
 *   If the $library array belongs to a certain version, a string containing the
 *   version.
 * @param string $variant
 *   If the $library array belongs to a certain variant, a string containing the
 *   variant name.
 */
function commerce_moip_payment_libraries_post_load_callback($library, $version = NULL, $variant = NULL) {
  //code here if needed
}


/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_moip_payment_commerce_payment_method_info() {
  $payment_methods['commerce_moip_payment'] = [
    'title' => t('Commerce MoIP'),
    'short_title' => t('MoIP'),
    'description' => t('MoIP payment gateway'),
    'terminal' => FALSE,
    'offsite' => FALSE,
    'active' => FALSE,
  ];
  return $payment_methods;
}


/**
 * Payment method settings form.
 *
 * @param $settings
 *   Default settings provided from rules
 *
 * @return array
 *   Settings form array
 */
function commerce_moip_payment_settings_form($settings) {
  //@todo check settings for moip
  $form = [];
  /*$currencies = commerce_currencies(TRUE);
  $currency_list = array();

  foreach ($currencies as $currency_code => $currency) {
    $currency_list[$currency_code] = $currency['name'];
  }

  $form['moip_currency'] = array(
    '#type' => 'select',
    '#title' => t('Currency'),
    '#options' => $currency_list,
    '#description' => t('Select the currency that you are using.'),
    '#default_value' => !empty($settings['currency']) ? $settings['currency'] : 'USD',
  );*/

  $form['endpoint'] = [
    '#type' => 'select',
    '#title' => t('Endpoint'),
    '#description' => t('Choose MoIP endpoint: '),
    '#options' => ['sandbox' => t('Sandbox'), 'production' => t('Production')],
    '#default_value' => !empty($settings['endpoint']) ? $settings['endpoint'] : COMMERCE_MOIP_PAYMENT_ENDPOINT,
  ];

  $form['authentication_type'] = [
    '#type' => 'select',
    '#title' => t('Authentication type'),
    '#description' => t('Choose MoIP authentication method: '),
    '#options' => ['basic' => t('Basic Auth'), 'oauth' => t('OAuth')],
    '#default_value' => !empty($settings['authentication_type']) ? $settings['authentication_type'] : COMMERCE_MOIP_PAYMENT_DEFAULT_AUTH_TYPE,
  ];

  $form['token'] = [
    '#type' => 'textfield',
    '#title' => t('Token'),
    '#description' => t('Token API Key. Get your key from https://moip.com.br'),
    '#default_value' => !empty($settings['token']) ? $settings['token'] : '',
    '#states' => [
      'visible' => [
        ':input[name$="[authentication_type]"]' => ['value' => 'basic'],
      ],
      'required' => [
        ':input[name$="[authentication_type]"]' => ['value' => 'basic'],
      ],
    ],
  ];
  $form['key'] = [
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#description' => t('API Key. Get your key from hhttps://moip.com.br'),
    '#default_value' => !empty($settings['key']) ? $settings['key'] : '',
    '#states' => [
      'visible' => [
        ':input[name$="[authentication_type]"]' => ['value' => 'basic'],
      ],
      'required' => [
        ':input[name$="[authentication_type]"]' => ['value' => 'basic'],
      ],
    ],
  ];

  $form['access_token'] = [
    '#type' => 'textfield',
    '#title' => t('OAuth Access Token'),
    '#description' => t('OAuth Access Token API Key. Get your key from https://moip.com.br'),
    '#default_value' => !empty($settings['access_token']) ? $settings['access_token'] : '',
    '#states' => [
      'visible' => [
        ':input[name$="[authentication_type]"]' => ['value' => 'oauth'],
      ],
      'required' => [
        ':input[name$="[authentication_type]"]' => ['value' => 'oauth'],
      ],
    ],
  ];

  $form['public_key'] = [
    '#type' => 'textarea',
    '#title' => t('Public Key'),
    '#description' => t('Public Key API Key. Needed for MoIP js card hashing. Get your key from https://moip.com.br'),
    '#default_value' => !empty($settings['public_key']) ? $settings['public_key'] : '',
  ];

  $form['txn_type'] = [
    '#type' => 'radios',
    '#title' => t('Default credit card transaction type'),
    '#description' => t('The default will be used to process transactions during checkout.'),
    '#options' => [
      COMMERCE_CREDIT_AUTH_CAPTURE => t('Authorization and capture'),
      COMMERCE_CREDIT_AUTH_ONLY => t('Authorization only (requires manual capture after checkout)'),
    ],
    '#default_value' => isset($settings['txn_type']) ? $settings['txn_type'] : COMMERCE_CREDIT_AUTH_CAPTURE,
  ];

  return $form;
}


/**
 * Payment method callback: checkout and terminal form.
 */
function commerce_moip_payment_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form = _commerce_moip_payment_elements_form($payment_method, $pane_values, $checkout_pane, $order);
  return $form;
}

/**
 * Callback for the MoIP Credit card form.
 */
function _commerce_moip_payment_elements_form($payment_method, $pane_values, $checkout_pane, $order) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  $credit_card_fields = [
    'number' => '',
    'exp_month' => '',
    'exp_year' => '',
    'code' => '',
  ];

  $form = commerce_payment_credit_card_form($credit_card_fields);

  $form['credit_card']['encrypted_hash'] = [
    '#type' => 'hidden',
    '#default_value' => '',
    '#attributes' => [
      'class' => ['moip-encrypted-hash'],
    ],
  ];
  $form['credit_card']['#attached']['js'][] = [
    'type' => 'setting',
    'data' => ['moipPublicKey' => $payment_method['settings']['public_key']],
  ];
  //we're checking cards via moip.min.js
  $form['credit_card']['#attached']['js'][] = [
    'type' => 'external',
    'data' => '//assets.moip.com.br/v2/moip.min.js',
  ];
  $form['credit_card']['#attached']['js'][] = [
    'data' => drupal_get_path('module', 'commerce_moip_payment') . '/js/commerce_moip_payment.js',
    'type' => 'file',
  ];


  // Add a css class so that we can easily identify related input fields
  // Do not require the fields
  //
  // Remove "name" attributes from  related input elements to
  // prevent card data to be sent to Drupal server
  foreach (array_keys($credit_card_fields) as $key) {
    $credit_card_field = &$form['credit_card'][$key];
    $credit_card_field['#attributes']['class'][] = 'moip-' . str_replace('_', '-', $key);
    $credit_card_field['#attributes']['class'][] = 'moip-change';
    $credit_card_field['#required'] = FALSE;
  }

  return $form;
}

/**
 * Payment method callback: submit form validation.
 */
function commerce_moip_payment_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = []) {
  // Validate the credit card fields.
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  $settings = [
    'form_parents' => array_merge($form_parents, ['credit_card']),
  ];

  $cardValidated = FALSE;
  //check if credit card is valid on moip (You have some hash from Moip )
  if (isset($pane_values['credit_card']['encrypted_hash']) && !empty($pane_values['credit_card']['encrypted_hash'])) {
    $cardValidated = TRUE;
  }

  if (!$cardValidated) {
    drupal_set_message('Credit card incorrect', 'error');
    return FALSE;
  }
}

/**
 * Payment method callback: submit form submission.
 */
function commerce_moip_payment_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {

  //@todo implement card on file below


  // The card is new.  Either charge and forget, or charge and save.
  if (!commerce_moip_payment_load_library()) {
    drupal_set_message(t('Error making the payment. Please contact shop admin to proceed.'), 'error');
    return FALSE;
  }


  // Just as an example, we might store information in the order object from the
  // payment parameters, though we would never save a full credit card number,
  // even in examples!

  $order->data['commerce_moip_payment'] = $pane_values;
  $order->data['encrypted_hash'] = $pane_values['credit_card']['encrypted_hash'];

  // Every attempted transaction should result in a new transaction entity being
  // created for the order to log either the success or the failure.
  //commerce_moip_transaction($payment_method, $order, $data, COMMERCE_PAYMENT_STATUS_PENDING,TRUE);
  $data = [];
  if ($charge['amount'] > 0) {
    /** @var \Moip\Moip $moip */
    $moip = commerce_moip_payment_connection();
    $hash = $pane_values['credit_card']['encrypted_hash'];
    $payment = FALSE;
    if ($moip instanceof Moip) {
      try {

        //generate data for customer order and card holder information so that we can react on it
        $data = commerce_moip_payment_prepare_data($order);

        //get customer or create one
        $customer = commerce_moip_payment_get_customer($moip, $order, $data);

        //get holder
        $holder = commerce_moip_payment_get_holder($moip, $data);

        //create order
        $moip_order = FALSE;
        // Creating an order
        if ($customer instanceof \Moip\Resource\Customer) {
          module_load_include('inc','commerce_moip_payment','includes/commerce_moip_payment.order');
          $moip_order = commerce_moip_payment_create_order($moip, $data, $order, $customer);
        }

        // Creating payment to order
        if ($moip_order instanceof \Moip\Resource\Orders && $holder instanceof \Moip\Resource\Holder) {

          //perform pre auth or auth and capture based on settings on payment method
          $payment = commerce_moip_payment_charge($moip_order,$holder,$hash,$payment_method['settings']);

          if ($payment instanceof \Moip\Resource\Payment) {
            // @todo improve logging of the change in transaction

            $status = commerce_moip_payment_status_to_commerce_status($payment);

            $message = t('Payment pre-authorization performed');

            if($payment_method['settings']['txn_type'] === COMMERCE_CREDIT_AUTH_CAPTURE){
              $message = t('Payment authorization and capture performed');
            }

            commerce_moip_payment_transaction($payment_method, $order, $data, $status, TRUE, $message , [], $payment);

            if ($status == COMMERCE_PAYMENT_STATUS_FAILURE) {
              return FALSE;
            }
          }
        }

        return TRUE;

      } catch (\Moip\Exceptions\UnautorizedException $e) {
        commerce_moip_payment_transaction($payment_method, $order, $data, COMMERCE_PAYMENT_STATUS_FAILURE, TRUE, $e->getMessage(), [], $payment);
        drupal_set_message($e->getMessage(), 'error');
        return FALSE;
      } catch (\Moip\Exceptions\ValidationException $e) {
        commerce_moip_payment_transaction($payment_method, $order, $data, COMMERCE_PAYMENT_STATUS_FAILURE, TRUE, $e->__toString(), [], $payment);
        drupal_set_message($e->__toString(), 'error');
        return FALSE;
      } catch (\Moip\Exceptions\UnexpectedException $e) {
        commerce_moip_payment_transaction($payment_method, $order, $data, COMMERCE_PAYMENT_STATUS_FAILURE, TRUE, $e->getMessage(), [], $payment);
        drupal_set_message(t('Unexpected error. Please contact us if you continue to have problems with entering your data'), 'error');
        return FALSE;
      } catch (Exception $e) {
        drupal_set_message(t('Unexpected error. Please contact us if you continue to have problems with entering your data'), 'error');
        return FALSE;
      }
    }
  }
}

/**
 * Creates an example payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 * @param $status
 *   Commerce Payment status
 * @param bool $save_transaction
 *   Save transaction (default TRUE)
 * @param string|bool $message (default FALSE)
 *   Transaction Message if you want to alter default
 * @param array|bool $message_variables
 *   Message variables if you have message
 * @param \Moip\Resource\Payment $payment
 *   Payment object
 *
 * @return \stdClass $transaction
 *   Transaction
 */
function commerce_moip_payment_transaction($payment_method, $order, $data, $status = COMMERCE_PAYMENT_STATUS_SUCCESS, $save_transaction = TRUE, $message = FALSE, $message_variables = FALSE, $payment = FALSE) {

  $card_details = $order->data['commerce_moip_payment']['credit_card'];

  $transaction = commerce_payment_transaction_new('commerce_moip_payment', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $data['total'];
  $transaction->currency_code = 'BRL';//this is converted to BRL in preparation of data
  $transaction->status = $status;
  if ($payment instanceof \Moip\Resource\Payment) {
    $transaction->remote_id = $payment->getId();
    $transaction->remote_status = $payment->getStatus();
  }
  $transaction->message = $message ? $message : 'Number: @number<br/>Expiration: @month/@year';
  $transaction->message_variables = $message_variables ? $message_variables : [
    '@number' => $card_details['number'],
    '@month' => $card_details['exp_month'],
    '@year' => $card_details['exp_year'],
  ];
  if ($save_transaction) {
    commerce_payment_transaction_save($transaction);
  }
  return $transaction;
}


/**
 * Gets or creates a customer if it doesn't exist on MoIP
 *
 * @param \Moip\Moip $moip
 * @param \stdClass $order
 *
 * @return $customer
 */
function commerce_moip_payment_get_customer($moip, $order, $data) {
  commerce_moip_payment_load_library();
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  $customer = FALSE;

  $ownId = $wrapper->uid->value();

  if($ownId != 0){
    //check if customer exists on MoIP by name
    $response = $moip->customers()
      ->getByPathNoPopulate(sprintf('/%s/%s?q=%s', \Moip\Resource\MoipResource::VERSION, \Moip\Resource\Customer::PATH, $data['billing']['name_line']));
    if (isset($response->customers)) {
      foreach ($response->customers as $customer) {
        if ($customer->ownId == $wrapper->uid->value()) {
          $customer = $moip->customers()->get($customer->id);
          break;
        }
      }
    }
  }

  if ($customer instanceof \Moip\Resource\Customer) {
    $customer->setFullname($data['billing']['name_line'])
      ->setEmail($wrapper->mail->value())
      ->setBirthDate($data['birth_date'])
      ->setTaxDocument($data['tax_document'], $data['tax_document_type'])
      ->setPhone($data['phone']['area_code'], $data['phone']['number'])
      ->addAddress(
        \Moip\Resource\Customer::ADDRESS_BILLING,
        $data['billing']['thoroughfare'],
        '',
        $data['billing']['dependent_locality'],
        $data['billing']['locality'],
        $data['billing']['administrative_area'],
        $data['billing']['postal_code'],
        $data['billing']['premise'],
        $data['billing']['country'])
      ->addAddress(
        \Moip\Resource\Customer::ADDRESS_SHIPPING,
        $data['shipping']['thoroughfare'],
        '',
        $data['shipping']['dependent_locality'],
        $data['shipping']['locality'],
        $data['shipping']['administrative_area'],
        $data['shipping']['postal_code'],
        $data['shipping']['premise'],
        $data['shipping']['country']);
  }
  else {
    // customer doesn't exist and we need to create it from order owner
    if($ownId == 0){
      $ownId = uniqid();
    }
    $customer = $moip->customers()->setOwnId($ownId)
      ->setFullname($data['billing']['name_line'])
      ->setEmail($wrapper->mail->value())
      ->setBirthDate($data['birth_date'])
      ->setTaxDocument($data['tax_document'], $data['tax_document_type'])
      ->setPhone($data['phone']['area_code'], $data['phone']['number'])
      ->addAddress(
        \Moip\Resource\Customer::ADDRESS_BILLING,
        $data['billing']['thoroughfare'],
        '',
        $data['billing']['dependent_locality'],
        $data['billing']['locality'],
        $data['billing']['administrative_area'],
        $data['billing']['postal_code'],
        $data['billing']['premise'],
        $data['billing']['country'])
      ->addAddress(
        \Moip\Resource\Customer::ADDRESS_SHIPPING,
        $data['shipping']['thoroughfare'],
        '',
        $data['shipping']['dependent_locality'],
        $data['shipping']['locality'],
        $data['shipping']['administrative_area'],
        $data['shipping']['postal_code'],
        $data['shipping']['premise'],
        $data['shipping']['country'])
      ->create();
  }

  return $customer;
}

/**
 * @param \Moip\Moip $moip
 * @param array $data
 *
 * @example
 * $holder = $moip->holders()->setFullname($billing_address['name_line'])
 * ->setBirthDate("1990-10-10")
 * ->setTaxDocument('22222222222', 'CPF') CPF or CNPJ
 * ->setPhone(11, 66778899, 55)
 * ->setAddress('BILLING', 'Avenida Faria Lima', '2927', 'Itaim', 'Sao Paulo',
 *   'SP', '01234000', 'Apt 101');
 *
 * @return mixed
 */
function commerce_moip_payment_get_holder($moip, $data) {
  commerce_moip_payment_load_library();

  $holder = $moip->holders()->setFullname($data['billing']['name_line'])
    ->setBirthDate($data['birth_date'])
    ->setTaxDocument($data['tax_document'], $data['tax_document_type'])
    ->setPhone($data['phone']['area_code'], $data['phone']['number'], $data['phone']['country_code'])
    ->setAddress(
      'BILLING',
      $data['billing']['thoroughfare'],
      '',
      $data['billing']['dependent_locality'],
      $data['billing']['locality'],
      $data['billing']['administrative_area'],
      $data['billing']['postal_code'],
      $data['billing']['premise']);

  return $holder;
}

/**
 * Map MoIP status to Commerce status
 * Possible values: CREATED, WAITING, IN_ANALYSIS, PRE_AUTHORIZED, AUTHORIZED,
 * CANCELLED, REFUNDED, REVERSED, SETTLED
 *
 * @param \Moip\Resource\Payment $payment
 *
 * @return string Commerce Payment status
 */
function commerce_moip_payment_status_to_commerce_status($payment) {
  if ($payment instanceof \Moip\Resource\Payment) {
    switch ($payment->getStatus()) {
      case 'CREATED':
      case 'WAITING':
      case 'PRE_AUTHORIZED':
      case 'IN_ANALYSIS':
        return COMMERCE_PAYMENT_STATUS_PENDING;
        break;
      case 'CANCELLED':
        return COMMERCE_PAYMENT_STATUS_FAILURE;
        break;
      case 'REFUNDED':
      case 'REVERSED':
      case 'SETTLED':
      case 'AUTHORIZED':
        return COMMERCE_PAYMENT_STATUS_SUCCESS;
        break;
    }
  }
  return COMMERCE_PAYMENT_STATUS_PENDING;
}

/**
 * Prepare data for sending to MoIP
 *
 * @param \stdClass $order
 *  Order
 *
 * @return array $data
 *  Data for sending to MoIP
 */
function commerce_moip_payment_prepare_data($order) {
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  //get Billing address from order
  $billing_address = $wrapper->commerce_customer_billing->commerce_customer_address->value();

  if (isset($wrapper->commerce_customer_shipping)) {
    $shipping_address = $wrapper->commerce_customer_shipping->commerce_customer_address->value();
  }
  else {
    $shipping_address = $billing_address;
  }

  //data for creation of customer
  $data = [];
  $data['billing'] = $billing_address;
  $data['shipping'] = $shipping_address;
  $data['birth_date'] = '';
  $data['tax_document'] = '';
  $data['tax_document_type'] = \Moip\Resource\Holder::TAX_DOCUMENT;
  $data['phone']['area_code'] = '';
  $data['phone']['number'] = '';
  $data['phone']['country_code'] = 55;
  $data['total_calculated'] = 0;

  $total_product_amount = 0;
  //add items from order
  foreach ($wrapper->commerce_line_items->getIterator() as $line_item) {
    if (isset($line_item->commerce_product)) {
      //convert this to BRL currency
      $amount = commerce_currency_convert((int) $line_item->commerce_total->amount->value(), $line_item->commerce_total->currency_code->value(), 'BRL');
      $total_product_amount += $amount;

      //we're placing unit price on order item for MoIP
      $unit_price_amount = commerce_currency_convert((int) $line_item->commerce_unit_price->amount->value(), $line_item->commerce_unit_price->currency_code->value(), 'BRL');
      $data['order']['items'][] = [
        'label' => $line_item->line_item_label->value(),
        'quantity' => (int) $line_item->quantity->value(),
        'sku' => $line_item->commerce_product->sku->value(),
        'amount' => $unit_price_amount,
        'category' => 'OTHER_CATEGORIES',
      ];
    }
  }
  $data['order']['addition'] = 0;
  $data['order']['shipping_amount'] = 0;
  $data['order']['original_currency_used'] = $wrapper->commerce_order_total->currency_code->value();//@todo check if this will be needed
  $data['total'] = commerce_currency_convert((int) $wrapper->commerce_order_total->amount->value(), $wrapper->commerce_order_total->currency_code->value(), 'BRL');
  $data['order']['discount'] = $total_product_amount - $data['total'];


  drupal_alter('commerce_moip_payment_prepare_data', $data, $order);

  return $data;
}

/**
 * Validate date format for MoIP
 *
 * @param $date
 * @param string $format
 *
 * @return bool
 */
function commerce_moip_payment_validate_birth_date($date, $format = 'Y-m-d') {
  $d = DateTime::createFromFormat($format, $date);
  return $d && $d->format($format) == $date;
}


/**
 * Access callback for processing returns.
 */
function commerce_moip_payment_refund_access($order, $transaction) {
  // Don't allow refunds on non-moip transactions.
  if ($transaction->payment_method != 'commerce_moip_payment') {
    return FALSE;
  }


  try {
    $moip = commerce_moip_payment_connection();
    $payment = $moip->payments()->get($transaction->remote_id);
    if ($payment->getStatus() == 'PRE_AUTHORIZED') {
      return FALSE;
    }
  } catch (Exception $e) {
    return FALSE;
  }

  return commerce_payment_transaction_access('update', $transaction);
}

/**
 * Access callback for processing capture.
 *
 * @param stdClass $order
 *  The commerce order entity.
 * @param stdClass $transaction
 *  The commerce payment transaction entity.
 *
 * @return bool
 *   Return TRUE if the user can update the transaction.
 */
function commerce_moip_payment_capture_access($order, $transaction) {
  // Only PRE_AUTHORIZED transactions can be captured.
  if ($transaction->payment_method != 'commerce_moip_payment' || empty($transaction->remote_id)) {
    return FALSE;
  }

  // Only PRE_AUTHORIZED and IN_ANALYSIS transaction can be captured.
  if(!in_array($transaction->remote_status,['IN_ANALYSIS','PRE_AUTHORIZED'])) {
    return FALSE;
  }

  // Return FALSE if it is more than 5 days past the original authorization.
  // MoIP does not allow capture after this time
  if (time() - $transaction->created > 86400 * 5) {
    return FALSE;
  }

  // Allow access if the user can update this transaction.
  return commerce_payment_transaction_access('update', $transaction);
}

/**
 * Access callback for voiding transactions.
 *
 * @param stdClass $order
 *  The commerce_order entity.
 * @param stdClass $transaction
 *  The commerce payment transaction entity.
 *
 * @return bool
 *  Return TRUE if the user can update the transaction.
 *
 */
function commerce_moip_payment_void_access($order, $transaction) {
  if ($transaction->payment_method != 'commerce_moip_payment' || empty($transaction->remote_id)) {
    return FALSE;
  }

  // Only PRE_AUTHORIZED and IN_ANALYSIS transaction can be voided.
  if(!in_array($transaction->remote_status,['IN_ANALYSIS','PRE_AUTHORIZED'])) {
    return FALSE;
  }

  // Allow access if the user can update this transaction.
  return commerce_payment_transaction_access('update', $transaction);
}

/**
 * Perform charge based on payment method settings
 *
 * @param \Moip\Resource\Orders $order
 *  MoIP order
 * @param \Moip\Resource\Holder $holder
 *  Credit card holder
 * @param $hash
 *  Credit card hash
 * @param $settings
 *  Payment method settings
 *
 * @return \Moip\Resource\Payment $payment
 * @throws \Exception
 */
function commerce_moip_payment_charge(\Moip\Resource\Orders $order, \Moip\Resource\Holder $holder , $hash, $settings){

  $payment = $order->payments()
    ->setCreditCardHash($hash, $holder)
    ->setDelayCapture()
    ->execute();

  if($settings['txn_type'] === COMMERCE_CREDIT_AUTH_CAPTURE){
    $payment->capture();
  }

  return $payment;
}

