Commerce MoIP Payment

Implementation of MoIP payment gateway for Drupal Commerce.

For module to work you need to add hook_commerce_moip_payment_prepare_data_alter

in your module and add in data array the following data from your customer fields:

- birth date,
- tax document
- tax document type
- phone number

other data can be changed but they are pre-propulated with order data.
